$(document).ready(function() {

  index_tabs('ul.b_submenu', 'section.main .wrap', '.b_text')

});

/* Functions */

function index_tabs (block_tabs, content_wrap, box) {
  $(block_tabs).find('a').click(function() {
    $(block_tabs).find('a').removeClass('active');
    $(this).addClass('active');
    $(content_wrap).find(box).addClass('hide');
    $(content_wrap).find(box + '[rel="' + $(this).attr('rel') + '"]').removeClass('hide');
    return false;
  });
}
